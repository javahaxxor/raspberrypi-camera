import sys, os, subprocess, time, glob, math, operator
from picamera.array import PiRGBArray
from PIL import Image
import time

# Inspired by https://www.pyimagesearch.com/2015/03/30/accessing-the-raspberry-pi-camera-with-opencv-and-python/
# allow the camera to warmup
time.sleep(0.1)

IMGPATH = "/run/shm/picam/"
LO_RES = " -w 640 -h 480"
HI_RES = " -w 1290 -h 960"
FULL_RES = " -w 2592 -h 1944"
TIMELAPSE = "raspistill -v -tl 50 -ex auto  --flicker auto  --vstab  -o " + IMGPATH + "image%d.jpg -e jpg" + LO_RES
CAPTURE = "raspistill -v -vf -n -o " + IMGPATH + "image%d.jpg -e jpg" + HI_RES
KILL = ["pkill", "-f", "raspistill"]
BASELINE_IMG_HIST = None


def rms(h1, h2):
    rmsdiff = math.sqrt(reduce(operator.add, map(lambda a, b: (a - b) ** 2, h1, h2)) / len(h1))
    return rmsdiff


def img_rms(baseline_hist, img2):
    return rms(baseline_hist, img2.split()[1].histogram())


def img_histogram(img):
    return img.split()[1].histogram()

try:
    print("Terminating previous raspistill processes")
    subprocess.Popen(KILL, shell=False)
    print("Working directory set to " + IMGPATH)

    if not os.path.exists(IMGPATH):
        print(IMGPATH + " doesn't exist. Trying to create")
        try:
            os.mkdir(IMGPATH)
        except IOError as e:
            print(e)
    elif not os.path.isdir(IMGPATH):
        os.remove(IMGPATH)
        os.mkdir(IMGPATH)

    #pTimelapse = subprocess.Popen(TIMELAPSE, shell=True)
    # wait until we have at least 2 image files
    while (True):
        step = 0;
        while True:
            files = filter(os.path.isfile, glob.glob(IMGPATH + "image*jpg"))
            print("Files to work on 1: " + repr(len(files)))

            if files > 1:
                break
            print "waiting for images"
            time.sleep(0.5)

        if pTimelapse.poll() is not None:
            print "pTimelapse.poll() is not None"
            print "restarting raspistill"
            pTimelapse = subprocess.Popen(TIMELAPSE, shell=True)

        files = filter(os.path.isfile, glob.glob(IMGPATH + "image*jpg"))
        files.sort(key=lambda x: os.path.getmtime(x))
        print("Files to work on: " + repr(len(files)))
        print("0 mtime:" + os.path.getmtime(files[0]))
        print("last mtime:" + os.path.getmtime(files[len(files)]))

        if len(files) == 0:
            print("break")

        # TODO: move firstImage forward every 5-10 minutes to compensate for sunlight intensity changes
        if BASELINE_IMG_HIST is None:
            firstImage = Image.open(files[0])
            print("first image: " + repr(firstImage))
            BASELINE_IMG_HIST = img_histogram(firstImage)

        for i in files:
            currentImage = Image.open(i)
            diff = img_rms(BASELINE_IMG_HIST, currentImage)
            print(i + "   " + repr(diff))
            if diff > 600:
                # pTimelapse=subprocess.Popen(CAPTURE,shell=True)
                print ("diff > 600 :: Save method here")

except (KeyboardInterrupt, SystemExit):
    pTimelapse.kill()
    raise
except ValueError as e:
    pTimelapse.kill()
    ' email someone '
    os.path.exists("/run/shm/")
    print e
    exit(1)
''' 
TODO:  clean up old files in both /run/shm to keep RAM free, and storage
send files to other units
email alert

'''
