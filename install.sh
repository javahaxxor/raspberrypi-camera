#!/bin/bash
sudo apt-get update && apt-get upgrade && apt-get install cmake pkg-config build-essential

sudo apt-get install opencv-python
# OpenCV needs to be able to load various image file formats from disk such as JPEG, PNG, TIFF, etc. 
sudo apt-get install libjpeg-dev libtiff5-dev libjasper-dev libpng12-dev

# OpenCV needs to be able to load various video formats
sudo apt-get install libavcodec-dev libavformat-dev libswscale-dev libv4l-dev
sudo apt-get install libxvidcore-dev libx264-dev

# Internally, the name of the module that handles OpenCV GUI operations is highgui . The highgui  module relies on the GTK library,
sudo apt-get install libgtk-3-dev
